// INCLUDE/*{{{*/

#include <cppunit/extensions/HelperMacros.h>

#include <Time/TestTime.hxx>
#include <Year/TestYear.hxx>

/*}}}*/

// Registering all unit tests here, to make them easier to disable and enable.

CPPUNIT_TEST_SUITE_REGISTRATION(TestTime);
CPPUNIT_TEST_SUITE_REGISTRATION(TestYear);

// Use no tabs at all; two spaces indentation; max. eighty chars per line.
// vim: et ts=2 sw=2 sts=2 tw=80 fdm=marker
