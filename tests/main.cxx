// INCLUDE/*{{{*/

#include <iostream>

#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/TextOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestRunner.h>

/*}}}*/

int main(int argc, char *argv[])
{
  CppUnit::TestResult test;
  CppUnit::TestResultCollector result;
  CppUnit::BriefTestProgressListener progress;
  test.addListener(&result);
  test.addListener(&progress);

  CppUnit::TextOutputter outputter(&result, std::clog);

  CppUnit::TestRunner runner;
  runner.addTest(CppUnit::TestFactoryRegistry::getRegistry().makeTest());
  runner.run(test);

  outputter.write();

  return result.wasSuccessful() ? 0 : 1;
}

// Use no tabs at all; two spaces indentation; max. eighty chars per line.
// vim: et ts=2 sw=2 sts=2 tw=80 fdm=marker
