/*{{{ LICENCE
 *
 * libdatetime
 * Copyright (C) 2009 Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *}}}*/

#include <TestYear.hxx>
#include <cppunit/extensions/HelperMacros.h>

#include <libdatetime/Year.hxx>
#include <sstream>
#include <string>
#include <set>

void TestYear::setUp()/*{{{*/
{
}

/*}}}*/
void TestYear::tearDown()/*{{{*/
{
}

/*}}}*/

void TestYear::testLeap()/*{{{*/
{
  std::set<int> leapYears;/*{{{*/
  leapYears.insert(1972);
  leapYears.insert(1976);
  leapYears.insert(1980);
  leapYears.insert(1984);
  leapYears.insert(1988);
  leapYears.insert(1992);
  leapYears.insert(1996);
  leapYears.insert(2000);
  leapYears.insert(2004);
  leapYears.insert(2008);
  leapYears.insert(2012);
  leapYears.insert(2016);
  leapYears.insert(2020);
  leapYears.insert(2024);
  leapYears.insert(2028);
  leapYears.insert(2032);
  leapYears.insert(2036);
  leapYears.insert(2040);
  leapYears.insert(2044);
  leapYears.insert(2048);
  leapYears.insert(2052);
  leapYears.insert(2056);
  leapYears.insert(2060);
  leapYears.insert(2064);
  leapYears.insert(2068);
  leapYears.insert(2072);
  leapYears.insert(2076);
  leapYears.insert(2080);
  leapYears.insert(2084);
  leapYears.insert(2088);
  leapYears.insert(2092);
  leapYears.insert(2096);

/*}}}*/
  std::set<int>::iterator it;
  for (int i = 1970; i <= 2100; ++i)
  {
    it = leapYears.find(i);
    bool isLeap = it != leapYears.end();

    libdatetime::Year y(i);
    CPPUNIT_ASSERT(y.isLeapYear() == isLeap);
  }
}

/*}}}*/
void TestYear::testDays()/*{{{*/
{
  libdatetime::Year leap(2000);
  CPPUNIT_ASSERT_EQUAL(static_cast<unsigned int>(366), leap.days());
  libdatetime::Year common(2007);
  CPPUNIT_ASSERT_EQUAL(static_cast<unsigned int>(365), common.days());
}

/*}}}*/
void TestYear::testSeconds()/*{{{*/
{
  libdatetime::Year leap(2000);
  CPPUNIT_ASSERT_EQUAL(static_cast<unsigned int>(366*86400), leap.seconds());
  libdatetime::Year common(2007);
  CPPUNIT_ASSERT_EQUAL(static_cast<unsigned int>(365*86400), common.seconds());
}

/*}}}*/
void TestYear::testConversion()/*{{{*/
{
  libdatetime::Year y(2000);
  int i = y;
  CPPUNIT_ASSERT_EQUAL(2000, i);
}

/*}}}*/
void TestYear::testArithmeticOperators()/*{{{*/
{
  CPPUNIT_FAIL("TODO: Add Timespan class/calculations.");
}

/*}}}*/
void TestYear::testAssignment()/*{{{*/
{
  libdatetime::Year y(0);
  y = 1234;
  CPPUNIT_ASSERT_EQUAL(1234, int(y));
}

/*}}}*/
void TestYear::testRelationalOperators()/*{{{*/
{
  libdatetime::Year lhs(42);

  CPPUNIT_ASSERT(lhs == 42);
  CPPUNIT_ASSERT(lhs <= 42);
  CPPUNIT_ASSERT(lhs >= 42);
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs != 42));
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs <  42));
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs >  42));

  CPPUNIT_ASSERT(lhs != 62);
  CPPUNIT_ASSERT(lhs <  62);
  CPPUNIT_ASSERT(lhs <= 62);
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs == 62));
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs >  62));
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs >= 62));

  libdatetime::Year rhse(42);
  libdatetime::Year rhsgt(62);

  CPPUNIT_ASSERT(lhs == rhse);
  CPPUNIT_ASSERT(lhs <= rhse);
  CPPUNIT_ASSERT(lhs >= rhse);
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs != rhse));
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs <  rhse));
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs >  rhse));

  CPPUNIT_ASSERT(lhs != rhsgt);
  CPPUNIT_ASSERT(lhs <  rhsgt);
  CPPUNIT_ASSERT(lhs <= rhsgt);
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs == rhsgt));
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs >  rhsgt));
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs >= rhsgt));
}

/*}}}*/
void TestYear::testCompoundAssignment()/*{{{*/
{
  CPPUNIT_FAIL("TODO: Add Timespan class/calculations.");
}

/*}}}*/
void TestYear::testInDecrement()/*{{{*/
{
  libdatetime::Year y(2000);

  CPPUNIT_ASSERT_EQUAL(2001, int(++y));
  y++;
  CPPUNIT_ASSERT_EQUAL(2002, int(y));
  CPPUNIT_ASSERT_EQUAL(2001, int(--y));
  y--;
  CPPUNIT_ASSERT_EQUAL(2000, int(y));
}

/*}}}*/

// Use no tabs at all; two spaces indentation; max. eighty chars per line.
// vim: et ts=2 sw=2 sts=2 tw=80 fdm=marker
