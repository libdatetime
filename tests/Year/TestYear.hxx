/*{{{ LICENCE
 *
 * libdatetime
 * Copyright (C) 2009 Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *}}}*/

#ifndef TESTS_TESTYEAR_HXX
#define TESTS_TESTYEAR_HXX

#include <cppunit/extensions/HelperMacros.h>

class TestYear : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(TestYear);
    CPPUNIT_TEST(testLeap);
    CPPUNIT_TEST(testDays);
    CPPUNIT_TEST(testSeconds);
    CPPUNIT_TEST(testConversion);
    CPPUNIT_TEST(testArithmeticOperators);
    CPPUNIT_TEST(testAssignment);
    CPPUNIT_TEST(testRelationalOperators);
    CPPUNIT_TEST(testCompoundAssignment);
    CPPUNIT_TEST(testInDecrement);
  CPPUNIT_TEST_SUITE_END();

  public:
    void setUp();
    void tearDown();

    void testLeap();
    void testDays();
    void testSeconds();
    void testConversion();
    void testArithmeticOperators();
    void testAssignment();
    void testRelationalOperators();
    void testCompoundAssignment();
    void testInDecrement();
};

#endif // TESTS_TESTYEAR_HXX

// Use no tabs at all; two spaces indentation; max. eighty chars per line.
// vim: et ts=2 sw=2 sts=2 tw=80 fdm=marker
