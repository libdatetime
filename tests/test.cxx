#include <iostream>
// DateTime.hxx
#include "DateTimeFormat.hxx"
#include "IsoDateTimeFormat.hxx"
#include "LocalDateTime.hxx"
#include "Timestamp.hxx"

int main()
{
    Timestamp ts;
    LocalDateTime dateTime(ts);
    DateTimeFormat format(&dateTime);
    IsoDateTimeFormat isoDefault(&dateTime);
    IsoDateTimeFormat isoNoDate(&dateTime, ~IsoDateTimeFormat::FORMAT_DATE);

    format.format("%c");
    // IsoDateTimeFormat isofalse(&dateTime);
    // isofalse.format("ERROR!"); // IsoDateTimeFormat::format() is private!

    std::cout <<  format.string() << std::endl;
    std::cout << isoDefault.string() << std::endl;
    std::cout << isoNoDate.string() << std::endl;

    LocalDateTime ldt(2008,  9,  1,23, 0, 0);
    IsoDateTimeFormat fmt(&ldt);
    std::cout << fmt.string() << std::endl;
    LocalDateTime ldt2(2008, 11, 01, 23, 0, 0);
    IsoDateTimeFormat fmt2(&ldt2);
    std::cout << fmt2.string() << std::endl;
    LocalDateTime ldt3(2008, 6, 01, 23, 0, 0);
    IsoDateTimeFormat fmt3(&ldt3);
    std::cout << fmt3.string() << std::endl;

    return 0;
}
