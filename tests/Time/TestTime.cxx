/*{{{ LICENCE
 *
 * libdatetime
 * Copyright (C) 2009 Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *}}}*/

#include <TestTime.hxx>
#include <cppunit/extensions/HelperMacros.h>

#include <libdatetime/Time.hxx>

void TestTime::setUp()/*{{{*/
{
}

/*}}}*/
void TestTime::tearDown()/*{{{*/
{
}

/*}}}*/
void TestTime::assert(libdatetime::Time& t,
            unsigned int hours, unsigned int minutes, unsigned int seconds)/*{{{*/
{
  CPPUNIT_ASSERT_EQUAL(hours,   t.hours()  );
  CPPUNIT_ASSERT_EQUAL(minutes, t.minutes());
  CPPUNIT_ASSERT_EQUAL(seconds, t.seconds());
}

/*}}}*/
void TestTime::testSeconds()/*{{{*/
{
  libdatetime::Time t(59);
  assert(t, 0, 0, 59);
}

/*}}}*/
void TestTime::testMinute()/*{{{*/
{
  libdatetime::Time t(60);
  assert(t, 0, 1, 0);
}

/*}}}*/
void TestTime::testMinutes()/*{{{*/
{
  libdatetime::Time t(3540);
  assert(t, 0, 59, 0);
}

/*}}}*/
void TestTime::testPreHour()/*{{{*/
{
  libdatetime::Time t(3599);
  assert(t, 0, 59, 59);
}

/*}}}*/
void TestTime::testHour()/*{{{*/
{
  libdatetime::Time t(3600);
  assert(t, 1, 0, 0);
}

/*}}}*/
void TestTime::testHours()/*{{{*/
{
  libdatetime::Time t(43200);
  assert(t, 12, 0, 0);
}

/*}}}*/
void TestTime::testConversion()/*{{{*/
{
  libdatetime::Time t(43266);
  int i = t;
  CPPUNIT_ASSERT_EQUAL(43266, i);
}

/*}}}*/
void TestTime::testArithmeticOperators()/*{{{*/
{
  CPPUNIT_FAIL("TODO: Add Timespan class/calculations.");
}

/*}}}*/
void TestTime::testAssignment()/*{{{*/
{
  libdatetime::Time t(42);
  t = 60;
  assert(t, 0, 1, 0);
}

/*}}}*/
void TestTime::testRelationalOperators()/*{{{*/
{
  libdatetime::Time lhs(42);

  CPPUNIT_ASSERT(lhs == 42);
  CPPUNIT_ASSERT(lhs <= 42);
  CPPUNIT_ASSERT(lhs >= 42);
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs != 42));
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs <  42));
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs >  42));

  CPPUNIT_ASSERT(lhs != 62);
  CPPUNIT_ASSERT(lhs <  62);
  CPPUNIT_ASSERT(lhs <= 62);
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs == 62));
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs >  62));
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs >= 62));

  libdatetime::Time rhse(42);
  libdatetime::Time rhsgt(62);

  CPPUNIT_ASSERT(lhs == rhse);
  CPPUNIT_ASSERT(lhs <= rhse);
  CPPUNIT_ASSERT(lhs >= rhse);
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs != rhse));
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs <  rhse));
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs >  rhse));

  CPPUNIT_ASSERT(lhs != rhsgt);
  CPPUNIT_ASSERT(lhs <  rhsgt);
  CPPUNIT_ASSERT(lhs <= rhsgt);
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs == rhsgt));
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs >  rhsgt));
  CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(lhs >= rhsgt));
}

/*}}}*/
void TestTime::testCompoundAssignment()/*{{{*/
{
  CPPUNIT_FAIL("TODO: Add Timespan class/calculations.");
}

/*}}}*/
void TestTime::testInDecrement()/*{{{*/
{
  libdatetime::Time inc(59);

  assert(++inc, 0, 1, 0);
  inc++;
  assert(inc, 0, 1, 1);

  libdatetime::Time dec(60);

  assert(--dec, 0, 0, 59);
  dec--;
  assert(dec, 0, 0, 58);
}

/*}}}*/

// Use no tabs at all; two spaces indentation; max. eighty chars per line.
// vim: et ts=2 sw=2 sts=2 tw=80 fdm=marker
