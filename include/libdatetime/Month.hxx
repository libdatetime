/*{{{ LICENCE
 *
 * libdatetime
 * Copyright (C) 2009 Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *}}}*/

#ifndef LIBDATETIME_MONTH_HXX
#define LIBDATETIME_MONTH_HXX

#include <libdatetime/Timestamp.hxx>

namespace libdatetime
{
  class Month
  {
    public:
      enum Type/*{{{*/
      {
        January = 1,
        February,
        March,
        April,
        May,
        June,
        July,
        August,
        September,
        October,
        November,
        December
      };

/*}}}*/
      // Month(Type month, bool isInLeapYear);/*{{{*/

      Month(Type month, bool isInLeapYear);

/*}}}*/
      // unsigned int days() const;/*{{{*/

      unsigned int days() const;

/*}}}*/
      // unsigned int seconds() const;/*{{{*/

      unsigned int seconds() const;

/*}}}*/
      // Type value() const;/*{{{*/

      Type value() const;

/*}}}*/
      // operator int() const;/*{{{*/

      operator int() const;

/*}}}*/
      // Month& operator=(Type rhs);/*{{{*/

      Month& operator=(Type rhs);

/*}}}*/
      // Month& operator++();/*{{{*/

      Month& operator++();

/*}}}*/
      // Month operator++(int noop);/*{{{*/

      Month operator++(int noop);

/*}}}*/
      // Month& operator--();/*{{{*/

      Month& operator--();

/*}}}*/
      // Month operator--(int noop);/*{{{*/

      Month operator--(int noop);

/*}}}*/

    protected:
      // void checkBoundaries(Type month)/*{{{*/

      void checkBoundaries(Type month);

/*}}}*/

    private:
      // Type _month;/*{{{*/

      Type _month;

/*}}}*/
      // bool _isInLeapYear;/*{{{*/

      bool _isInLeapYear;

/*}}}*/
  };

}

#endif // LIBDATETIME_MONTH_HXX

// Use no tabs at all; two spaces indentation; max. eighty chars per line.
// vim: et ts=2 sw=2 sts=2 tw=80 fdm=marker
