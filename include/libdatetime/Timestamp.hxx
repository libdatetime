/*{{{ LICENCE
 *
 * libdatetime
 * Copyright (C)  Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *}}}*/

#ifndef LIBDATETIME_TIMESTAMP_HXX
#define LIBDATETIME_TIMESTAMP_HXX

namespace libdatetime
{
  /**
   * @brief Represents an UNIX timestamp.
   *
   * Since points in time can not move around, the value of a
   * timestamp can not be altered if set once.
   */
  class Timestamp
  {
    public:
      // Timestamp();/*{{{*/

      /**
       * @brief Default constructor, makes a snapshot of current time.
       *
       * Stores the current UNIX timestamp as returned by time() of time.h.
       */
      Timestamp();

/*}}}*/
      // explicit Timestamp(int time);/*{{{*/

      /**
       * @brief Constructor to initialize object to a specific timestamp value.
       * @param time The value of an UNIX timestamp.
       */
      explicit Timestamp(int time);

/*}}}*/
      // virtual ~Timestamp();/*{{{*/

      /**
       * @brief Virtual destructor.
       */
      virtual ~Timestamp();

/*}}}*/
      // operator int() const;/*{{{*/

      operator int() const;

/*}}}*/
      // Timestamp& operator++()/*{{{*/

      Timestamp& operator++();

/*}}}*/
      // Timestamp operator++(int noop)/*{{{*/

      Timestamp operator++(int noop);

/*}}}*/
      // Timestamp& operator--()/*{{{*/

      Timestamp& operator--();

/*}}}*/
      // Timestamp operator--(int noop)/*{{{*/

      Timestamp operator--(int noop);

/*}}}*/
      // Timestamp& operator=(int rhs)/*{{{*/

      Timestamp& operator=(int rhs);

/*}}}*/
      // Timestamp& operator+=(int rhs)/*{{{*/

      Timestamp& operator+=(int rhs);

/*}}}*/
      // Timestamp& operator-=(int rhs)/*{{{*/

      Timestamp& operator-=(int rhs);

/*}}}*/

    private:
      // int _raw;/*{{{*/

      /// The raw value of this timestamp.
      int _raw;

/*}}}*/
  };
}

#endif // LIBDATETIME_TIMESTAMP_HXX

// Use no tabs at all; two spaces indentation; max. eighty chars per line.
// vim: et ts=2 sw=2 sts=2 tw=80 fdm=marker
