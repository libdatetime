/*{{{ LICENCE
 *
 * libdatetime
 * Copyright (C) 2009 Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *}}}*/

#ifndef LIBDATETIME_DATETIME_HXX
#define LIBDATETIME_DATETIME_HXX

#include <libdatetime/Timestamp.hxx>
#include <libdatetime/Time.hxx>
#include <libdatetime/Date.hxx>

namespace libdatetime
{
  /**
   * @brief Representation of date and time.
   */
  class DateTime
  {
    public:
      // DateTime(const Timestamp&);/*{{{*/

      /**
       * @brief Calculates date and time with the timestamp.
       */

      DateTime(const Timestamp&);

/*}}}*/
      // DateTime(Timestamp*);/*{{{*/

      /**
       * @brief Calculates date and time with the timestamp.
       * @note This passes the ownership.
       */
      DateTime(Timestamp*);

/*}}}*/
      // DateTime(const Date&, const Time&);/*{{{*/

      /**
       * @brief Constructs a DateTime out of a Date and Time.
       */

      DateTime(const Date&, const Time&);


/*}}}*/
      // DateTime(Date*, Time*);/*{{{*/

      /**
       * @brief Constructs a DateTime out of a Date and Time.
       * @note This passes the ownership.
       */
      DateTime(Date*, Time*);

/*}}}*/
      // virtual ~DateTime();/*{{{*/

      /**
       * @brief Virtual destructor.
       */
      virtual ~DateTime();

/*}}}*/
      // const Timestamp& timestamp() const;/*{{{*/

      /**
       * @brief Returns a timestamp resembling this date and time.
       */
      const Timestamp& timestamp() const;

/*}}}*/
      // const Date& date() const;/*{{{*/

      /**
       * @brief Returns the date.
       */
      const Date& date() const;

/*}}}*/
      // const Time& time() const;/*{{{*/

      /**
       * @brief Returns the time.
       */

      const Time& time() const;

/*}}}*/

    protected:
      // DateTime();/*{{{*/

      /**
       * @brief Default constructor.
       *
       * Protected since only child classes should create an object without
       * valid values set.
       *
       * @note You want to call one of the @ref set() methods in the constructor
       */
      DateTime();

/*}}}*/
      // void set(const Timestamp&);/*{{{*/

      /**
       * @brief Sets a date based on the supplied timestamp.
       */
      void set(const Timestamp&);

/*}}}*/
      // void set(Timestamp*);/*{{{*/

      /**
       * @brief Sets a date based on the supplied timestamp.
       * @note This passes the ownership.
       */
      void set(Timestamp*);

/*}}}*/
      // void set(const Date&, const Time&);/*{{{*/

      /**
       * @brief Sets a date based on the supplied date and time.
       */
      void set(const Date&, const Time&);

/*}}}*/
      // void set(Date*, Time*);/*{{{*/

      /**
       * @brief Sets a date based on the supplied date and time.
       * @note This passes the ownership.
       */
      void set(Date*, Time*);

/*}}}*/
      // void dispose();/*{{{*/

      /**
       * @brief Disposes all memory held and owned by this object.
       */
      void dispose();

/*}}}*/

    private:
      // Timestamp* _timestamp;/*{{{*/

      /// The Timestamp describing this date and time.
      const Timestamp* _timestamp;

/*}}}*/
      // bool _ownsTimestamp;/*{{{*/

      /// Whether @ref _timestamp has to be deleted upon destruction.
      bool _ownsTimestamp;

/*}}}*/
      // Date* _date;/*{{{*/

      /// The date behind the associated timestamp.
      const Date* _date;

/*}}}*/
      // bool _ownsDate;/*{{{*/

      /// whether @ref _date has to be deleted  upon destruction.
      bool _ownsDate;

/*}}}*/
      // Time* _time;/*{{{*/

      /// The time behind the associated timestamp.
      const Time* _time;

/*}}}*/
      // bool _ownsTime;/*{{{*/

      /// Whether @ref _time has to be deleted upon destruction.
      bool _ownsTime;

/*}}}*/

      // void _allocateTimestamp();/*{{{*/

      void _allocateTimestamp();

/*}}}*/
      // void _allocateDateTime();/*{{{*/

      void _allocateDateTime();

/*}}}*/
  };
}

#endif // LIBDATETIME_DATETIME_HXX

// Use no tabs at all; two spaces indentation; max. eighty chars per line.
// vim: et ts=2 sw=2 sts=2 tw=80 fdm=marker
