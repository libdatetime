/*{{{ LICENCE
 *
 * libdatetime
 * Copyright (C) 2009 Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *}}}*/

#ifndef LIBDATETIME_TIME_HXX
#define LIBDATETIME_TIME_HXX

namespace libdatetime
{
  class Time
  {
    public:
      // Time(unsigned int seconds);/*{{{*/

      Time(unsigned int seconds);

/*}}}*/
      // unsigned int hours() const;/*{{{*/

      unsigned int hours() const;

/*}}}*/
      // unsigned int minutes() const;/*{{{*/

      unsigned int minutes() const;

/*}}}*/
      // unsigned int seconds() const;/*{{{*/

      unsigned int seconds() const;

/*}}}*/
      // operator int() const;/*{{{*/

      operator int() const;

/*}}}*/
      // Time& operator++();/*{{{*/

      Time& operator++();

/*}}}*/
      // Time  operator++(int noop);/*{{{*/

      Time  operator++(int noop);

/*}}}*/
      // Time& operator--();/*{{{*/

      Time& operator--();

/*}}}*/
      // Time  operator--(int noop);/*{{{*/

      Time  operator--(int noop);

/*}}}*/

    private:
      // unsigned int _totalSeconds;/*{{{*/

      unsigned int _totalSeconds;

/*}}}*/
      // unsigned int _hours;/*{{{*/

      unsigned int _hours;

/*}}}*/
      // unsigned int _minutes;/*{{{*/

      unsigned int _minutes;

/*}}}*/
      // unsigned int _seconds;/*{{{*/

      unsigned int _seconds;

/*}}}*/
  };
}

#endif // LIBDATETIME_TIME_HXX

// Use no tabs at all; two spaces indentation; max. eighty chars per line.
// vim: et ts=2 sw=2 sts=2 tw=80 fdm=marker
