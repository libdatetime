/*{{{ LICENCE
 *
 * libdatetime
 * Copyright (C) 2009 Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *}}}*/

#ifndef LIBDATETIME_DATE_HXX
#define LIBDATETIME_DATE_HXX

#include <libdatetime/Timestamp.hxx>
#include <libdatetime/Year.hxx>
#include <libdatetime/Month.hxx>

namespace libdatetime
{
  /**
   * @brief Representation of a date.
   */
  class Date
  {
    public:
      // Date(const Timestamp& timestamp)/*{{{*/

      Date(const Timestamp& timestamp);

/*}}}*/
      // const Year& year() const/*{{{*/

      const Year& year() const;

/*}}}*/
      // const Month& month() const/*{{{*/

      const Month& month() const;

/*}}}*/
      // int day() const/*{{{*/

      int day() const;

/*}}}*/
      // operator int() const/*{{{*/

      operator int() const;

/*}}}*/

    protected:
      // void incrementYear()/*{{{*/

      void incrementYear();

/*}}}*/
      // void decrementYear()/*{{{*/

      void decrementYear();

/*}}}*/
      // void incrementMonth()/*{{{*/

      void incrementMonth();

/*}}}*/
      // void decrementMonth()/*{{{*/

      /// @see incrementMonth()
      void decrementMonth();

/*}}}*/
      // void incrementDay()/*{{{*/

      void incrementDay();

/*}}}*/
      // void decrementDay()/*{{{*/

      /// @see incrementDay()
      void decrementDay();

/*}}}*/

    private:
      // Timestamp _timestamp;/*{{{*/

      Timestamp _timestamp;

/*}}}*/
      // Year _year;/*{{{*/

      Year _year;

/*}}}*/
      // Month  _month;/*{{{*/

      Month  _month;

/*}}}*/
      // unsigned int _day;/*{{{*/

      unsigned int _day;

/*}}}*/
      // void _calculatePreEpoch();/*{{{*/

      void _calculatePreEpoch();

/*}}}*/
      // void _calculatePostEpoch();/*{{{*/

      void _calculatePostEpoch();

/*}}}*/
  };
}

#endif // LIBDATETIME_DATE_HXX

// Use no tabs at all; two spaces indentation; max. eighty chars per line.
// vim: et ts=2 sw=2 sts=2 tw=80 fdm=marker
