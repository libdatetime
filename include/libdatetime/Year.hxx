/*{{{ LICENCE
 *
 * libdatetime
 * Copyright (C) 2009 Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *}}}*/

#ifndef LIBDATETIME_YEAR_HXX
#define LIBDATETIME_YEAR_HXX

#include <libdatetime/Timestamp.hxx>
#include <iostream>

namespace libdatetime
{
  class Year
  {
    public:
      // explicit Year(int year);/*{{{*/

      explicit Year(int year);

/*}}}*/
      // bool isLeapYear() const;/*{{{*/

      /**
       * @brief Checks whether this year is a leap year.
       */
      bool isLeapYear() const;

/*}}}*/
      // unsigned int days() const;/*{{{*/

      // Usually 365, 366 in leap years.
      unsigned int days() const;

/*}}}*/
      // unsigned int seconds() const;/*{{{*/

      unsigned int seconds() const;

/*}}}*/
      // operator int() const/*{{{*/

      operator int() const;

/*}}}*/
      // Year& operator=(int rhs)/*{{{*/

      Year& operator=(int rhs);

/*}}}*/
      // Year& operator++()/*{{{*/

      Year& operator++();

/*}}}*/
      // Year operator++(int noop)/*{{{*/

      Year operator++(int noop);

/*}}}*/
      // Year& operator--()/*{{{*/

      Year& operator--();

/*}}}*/
      // Year operator--(int noop)/*{{{*/

      Year operator--(int noop);

/*}}}*/

    private:
      // int _year;/*{{{*/

      int _year;

/*}}}*/
  };

}

#endif // LIBDATETIME_YEAR_HXX

// Use no tabs at all; two spaces indentation; max. eighty chars per line.
// vim: et ts=2 sw=2 sts=2 tw=80 fdm=marker
