/*{{{ LICENCE
 *
 * libdatetime
 * Copyright (C) 2009 Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *}}}*/

#include <libdatetime/DateTime.hxx>

#include <libdatetime/Date.hxx>
#include <libdatetime/Time.hxx>

#include <cstdlib>

namespace libdatetime
{
  // public
  DateTime::DateTime(const Timestamp& timestamp)/*{{{*/
  : _timestamp(NULL)
  , _ownsTimestamp(false)
  , _date(NULL)
  , _ownsDate(false)
  , _time(NULL)
  , _ownsTime(false)
  {
    set(timestamp);
  }

/*}}}*/
  DateTime::DateTime(Timestamp* timestamp)/*{{{*/
  : _timestamp(NULL)
  , _ownsTimestamp(false)
  , _date(NULL)
  , _ownsDate(false)
  , _time(NULL)
  , _ownsTime(false)
  {
    set(timestamp);
  }

/*}}}*/
  DateTime::DateTime(const Date& date, const Time& time)/*{{{*/
  : _timestamp(NULL)
  , _ownsTimestamp(false)
  , _date(NULL)
  , _ownsDate(false)
  , _time(NULL)
  , _ownsTime(false)
  {
    set(date, time);
  }

/*}}}*/
  DateTime::DateTime(Date* date, Time* time)/*{{{*/
  : _timestamp(NULL)
  , _ownsTimestamp(false)
  , _date(NULL)
  , _ownsDate(false)
  , _time(NULL)
  , _ownsTime(false)
  {
    set(date, time);
  }

/*}}}*/
  DateTime::~DateTime()/*{{{*/
  {
    dispose();
  }

/*}}}*/
  const Timestamp& DateTime::timestamp() const/*{{{*/
  {
    return *_timestamp;
  }

/*}}}*/
  const Date& DateTime::date() const/*{{{*/
  {
    return *_date;
  }

/*}}}*/
  const Time& DateTime::time() const/*{{{*/
  {
    return *_time;
  }

/*}}}*/

  // protected
  DateTime::DateTime()/*{{{*/
  : _timestamp(NULL)
  , _ownsTimestamp(false)
  , _date(NULL)
  , _ownsDate(false)
  , _time(NULL)
  , _ownsTime(false)
  {
  }

/*}}}*/
  void DateTime::set(const Timestamp& timestamp)/*{{{*/
  {
    dispose();

    _timestamp     = &timestamp;
    _ownsTimestamp = false;

    _allocateDateTime();
  }

/*}}}*/
  void DateTime::set(Timestamp* timestamp)/*{{{*/
  {
    dispose();

    _timestamp     = timestamp;
    _ownsTimestamp = true;

    _allocateDateTime();
  }

/*}}}*/
  void DateTime::set(const Date& date, const Time& time)/*{{{*/
  {
    dispose();

    _date     = &date;
    _ownsDate = false;
    _time     = &time;
    _ownsTime = false;

    _allocateTimestamp();
  }

/*}}}*/
  void DateTime::set(Date* date, Time* time)/*{{{*/
  {
    dispose();

    _date     = date;
    _ownsDate = true;
    _time     = time;
    _ownsTime = true;

    _allocateTimestamp();
  }

/*}}}*/
  void DateTime::dispose()/*{{{*/
  {
    if (_ownsTimestamp) {
      delete _timestamp;
      _timestamp = NULL;
    }
    if (_ownsDate) {
      delete _date;
      _date = NULL;
    }
    if (_ownsTime) {
      delete _time;
      _time = NULL;
    }
  }

/*}}}*/

  // private
  void DateTime::_allocateTimestamp()/*{{{*/
  {
    _timestamp     = new Timestamp(abs(*_date + *_time));
    _ownsTimestamp = true;
  }

/*}}}*/
  void DateTime::_allocateDateTime()/*{{{*/
  {
    _date     = new Date(*_timestamp);
    _ownsDate = true;
    _time     = new Time(*_date - *_timestamp);
    _ownsTime = true;
  }

/*}}}*/
}

// Use no tabs at all; two spaces indentation; max. eighty chars per line.
// vim: et ts=2 sw=2 sts=2 tw=80 fdm=marker
