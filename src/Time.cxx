/*{{{ LICENCE
 *
 * libdatetime
 * Copyright (C) 2009 Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *}}}*/

#include <libdatetime/Time.hxx>

namespace libdatetime
{
  Time::Time(unsigned int seconds)/*{{{*/
  : _totalSeconds(seconds)
  , _hours(0)
  , _minutes(0)
  , _seconds(seconds)
  {
    _hours    = _seconds / (60 * 60);
    _seconds -= _hours   *  60 * 60;

    _minutes  = _seconds / 60;
    _seconds -= _minutes * 60;
  }

/*}}}*/
  unsigned int Time::hours() const/*{{{*/
  {
    return _hours;
  }

/*}}}*/
  unsigned int Time::minutes() const/*{{{*/
  {
    return _minutes;
  }

/*}}}*/
  unsigned int Time::seconds() const/*{{{*/
  {
    return _seconds;
  }

/*}}}*/
  Time::operator int() const/*{{{*/
  {
    return _totalSeconds;
  }

/*}}}*/
  Time& Time::operator++()/*{{{*/
  {
    if (59 == _seconds) {
      if (59 == _minutes) {
        ++_hours;
        _minutes = 0;
      } else {
        ++_minutes;
      }
      _seconds = 0;
    } else {
      ++_seconds;
    }

    return *this;
  }

/*}}}*/
  Time Time::operator++(int noop)/*{{{*/
  {
    Time pre(*this);
    ++(*this);

    return pre;
  }

/*}}}*/
  Time& Time::operator--()/*{{{*/
  {
    if (0 == _seconds) {
      if (0 == _minutes) {
        --_hours;
        _minutes = 59;
      } else {
        --_minutes;
      }
      _seconds = 59;
    } else {
      --_seconds;
    }

    return *this;
  }

/*}}}*/
  Time Time::operator--(int noop)/*{{{*/
  {
    Time pre(*this);
    --(*this);

    return pre;
  }

/*}}}*/
}

// Use no tabs at all; two spaces indentation; max. eighty chars per line.
// vim: et ts=2 sw=2 sts=2 tw=80 fdm=marker
