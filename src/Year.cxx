/*{{{ LICENCE
 *
 * libdatetime
 * Copyright (C) 2009 Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *}}}*/

#include <libdatetime/Year.hxx>

#include <libdatetime/Timestamp.hxx>


namespace libdatetime
{
  Year::Year(int year)/*{{{*/
  : _year(year)
  {
  }

/*}}}*/
  bool Year::isLeapYear() const/*{{{*/
  {
    // Year divisible by 4, but not by
    // 100 except it is also divisible by 400).
    return (_year % 4 == 0 && _year % 100 != 0 || _year % 400 == 0);
  }

/*}}}*/
  unsigned int Year::days() const/*{{{*/
  {
    return isLeapYear() ? 366 : 365;
  }

/*}}}*/
  unsigned int Year::seconds() const/*{{{*/
  {
    return days() * 24 * 60 * 60;
  }

/*}}}*/
  Year::operator int() const/*{{{*/
  {
    return _year;
  }

/*}}}*/
  Year& Year::operator=(int rhs)/*{{{*/
  {
    _year = rhs;
    return *this;
  }

/*}}}*/
  Year& Year::operator++()/*{{{*/
  {
    ++_year;
    return *this;
  }

/*}}}*/
  Year Year::operator++(int noop)/*{{{*/
  {
    Year pre(_year);
    ++_year;
    return pre;
  }

/*}}}*/
  Year& Year::operator--()/*{{{*/
  {
    --_year;
    return *this;
  }

/*}}}*/
  Year Year::operator--(int noop)/*{{{*/
  {
    Year pre(_year);
    --_year;
    return pre;
  }

/*}}}*/
}

// Use no tabs at all; two spaces indentation; max. eighty chars per line.
// vim: et ts=2 sw=2 sts=2 tw=80 fdm=marker
