/*{{{ LICENCE
 *
 * libdatetime
 * Copyright (C) 2009 Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *}}}*/

#include <libdatetime/Month.hxx>

namespace libdatetime
{
  Month::Month(Type month, bool isInLeapYear)/*{{{*/
  : _month(month)
  , _isInLeapYear(isInLeapYear)
  {
    checkBoundaries(month);
  }

/*}}}*/
  unsigned int Month::days() const/*{{{*/
  {
    if (Month::February == _month) {
      // February has 28 days, 29 in leap years.
      return (_isInLeapYear ? 29 : 28);
    } else if (Month::August > _month) {
      // January to July have 30 days on even month number
      // and 31 on odd.
      return 30 + _month % 2;
    } else {
      // August to December have 31 days on even month number
      // and 30 on odd.
      return 31 - _month % 2;
    }
  }

/*}}}*/
  unsigned int Month::seconds() const/*{{{*/
  {
    return days() * 86400;
  }

/*}}}*/
  Month::Type Month::value() const/*{{{*/
  {
    return _month;
  }

/*}}}*/
  Month::operator int() const/*{{{*/
  {
    return _month;
  }

/*}}}*/
  Month& Month::operator=(Type rhs)/*{{{*/
  {
    checkBoundaries(rhs);
    _month = rhs;

    return *this;
  }

/*}}}*/
  Month& Month::operator++()/*{{{*/
  {
    if (Month::December == _month) {
      throw "Cannot increment month: already december";
    }

    _month = static_cast<Month::Type>(static_cast<int>(_month) + 1);

    return *this;
  }

/*}}}*/
  Month Month::operator++(int noop)/*{{{*/
  {
    Month pre(*this);
    ++(*this);
    return pre;
  }

/*}}}*/
  Month& Month::operator--()/*{{{*/
  {
    if (Month::January == _month) {
      throw "Cannot decrement month: already January";
    }

    _month = static_cast<Month::Type>(static_cast<int>(_month) - 1);

    return *this;
  }

/*}}}*/
  Month Month::operator--(int noop)/*{{{*/
  {
    Month pre(*this);
    --(*this);
    return pre;
  }

/*}}}*/

  void Month::checkBoundaries(Type month)/*{{{*/
  {
    if (month < Month::January || month > Month::December) {
      throw "Not a month";
    }
  }

/*}}}*/

}

// Use no tabs at all; two spaces indentation; max. eighty chars per line.
// vim: et ts=2 sw=2 sts=2 tw=80 fdm=marker
