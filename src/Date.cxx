/*{{{ LICENCE
 *
 * libdatetime
 * Copyright (C) 2009 Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *}}}*/

#include <libdatetime/Date.hxx>

#include <libdatetime/Timestamp.hxx>
#include <libdatetime/Month.hxx>
#include <libdatetime/Year.hxx>

namespace libdatetime
{
  Date::Date(const Timestamp& timestamp)/*{{{*/
  : _timestamp(timestamp)
  , _year(1970)
  , _month(Month::January, _year.isLeapYear())
  , _day(1)
  {
    if (_timestamp < Timestamp(0)) {
      _calculatePreEpoch();
    } else if (_timestamp > 0) {
      _calculatePostEpoch();
    }
  }

/*}}}*/
  const Year& Date::year() const/*{{{*/
  {
    return _year;
  }

/*}}}*/
  const Month& Date::month() const/*{{{*/
  {
    return _month;
  }

/*}}}*/
  int Date::day() const/*{{{*/
  {
    return _day;
  }

/*}}}*/
  Date::operator int() const/*{{{*/
  {
    return _timestamp;
  }

/*}}}*/
  void Date::incrementYear()/*{{{*/
  {
    ++_year;
  }

/*}}}*/
  void Date::decrementYear()/*{{{*/
  {
    --_year;
  }

/*}}}*/
  void Date::incrementMonth()/*{{{*/
  {
    // We can't blindly increment the month since the year changes after
    // december.
    // This is scenario does only occur if the Epoch does not begin in
    // january.
    if (_month == Month::December) {
      ++_year;
      _month = Month(Month::January, _year.isLeapYear());
    } else {
      ++_month;
    }
  }

/*}}}*/
  void Date::decrementMonth()/*{{{*/
  {
    // See incrementMonth() for explanation.
    if (_month == Month::January) {
      --_year;
      _month = Month(Month::December, _year.isLeapYear());
    } else {
      --_month;
    }
  }

/*}}}*/
  void Date::incrementDay()/*{{{*/
  {
    // The month has to be incremented if the last day of the month is
    // reached and shall be incremented. This happens only when the Epoch
    // does not start on the first of any month.
    if (_month.days() == _day) {
      incrementMonth();
      _day = 1;
    } else {
      ++_day;
    }
  }

/*}}}*/
  void Date::decrementDay()/*{{{*/
  {
    // See incrementDay() for explanation.
    if (1 == _day) {
      decrementMonth();
      _day = _month.days();
    } else {
      --_day;
    }
  }

/*}}}*/
  void Date::_calculatePreEpoch()/*{{{*/
  {
    unsigned int remainder = 0 - _timestamp;

    // As long as there is at least the same amount of seconds remaining than
    // this previous year had, substract the seconds of that year from the
    // remainder and decrease the year number.

    while (true) {
      decrementYear();
      if (remainder < _year.seconds()) {
        incrementYear();
        break;
      }
      remainder -= _year.seconds();
    }

    // Calculate the month.
    while (true) {
      decrementMonth();
      if (remainder < _month.seconds()) {
        incrementMonth();
        break;
      }
      remainder -= _month.seconds();
    }

    // Calculate the day of the month.
    while (remainder >= 24 * 60 * 60) {
      remainder -= 24 * 60 * 60;
      decrementDay();
    }

    if (0 == remainder) return;

    // We have a day. But if there is at least one second left to substract,
    // the day has to be decremented as the time of the days is 00:00.
    decrementDay();
    _timestamp -= 24 * 60 * 60;

    // We have to substract the time from the initial timestamp to obtain the
    // actual timestamp of this date.
    _timestamp += remainder;
  }

/*}}}*/
  void Date::_calculatePostEpoch()/*{{{*/
  {
    unsigned int remainder = _timestamp;
    // Calculate the year. Note that it can also be altered in the loops below
    // this one.

    // As long as there is at least the same amount of seconds remaining than
    // this current year has, substract the seconds of this year from the
    // remainder and adjust the year.
    while (remainder >= _year.seconds()) {
      remainder -= _year.seconds();
      incrementYear();
    }

    // Calculate the month.
    while (remainder >= _month.seconds()) {
      remainder -= _month.seconds();
      incrementMonth();
    }

    // Calculate the day of the month.
    while (remainder >= 24 * 60 * 60) {
      remainder -= 24 * 60 * 60;
      incrementDay();
    }

    // The Date has been calculated. What remains are the seconds that have
    // passed since 00:00 this day, aka "the time". We have to substract the
    // time from the initial timestamp to obtain the actual timestamp of this
    // date.
    _timestamp -= remainder;
  }

/*}}}*/
}

// Use no tabs at all; two spaces indentation; max. eighty chars per line.
// vim: et ts=2 sw=2 sts=2 tw=80 fdm=marker
